module Operands exposing (main)

import Bitwise exposing (and, shiftLeftBy, shiftRightZfBy)
import Browser
import Html exposing (Html, table, tbody, td, text, th, thead, tr)
import Html.Attributes exposing (class, colspan)
import Html.Events exposing (onClick)
import Word.Hex


type Msg
    = BitFlip Int Int
    | OperatorSwitch


type Operator
    = And
    | Or
    | Xor
    | Not


type alias Model =
    { operands : List Int
    , operator : Operator
    }


type alias Flags =
    { operation : String }


toHex : Int -> String
toHex value =
    "0x" ++ Word.Hex.fromInt 4 value


init : Flags -> ( Model, Cmd msg )
init flags =
    ( { operands = [ 0x5555, 0x3333 ]
      , operator =
            case flags.operation of
                "AND" ->
                    And

                "OR" ->
                    Or

                "XOR" ->
                    Xor

                "NOT" ->
                    Not

                _ ->
                    And
      }
    , Cmd.none
    )


operatorName : Model -> String
operatorName model =
    case model.operator of
        And ->
            "AND"

        Or ->
            "OR"

        Xor ->
            "XOR"

        Not ->
            "NOT"


update : Msg -> Model -> ( Model, Cmd msg )
update msg model =
    case msg of
        BitFlip opNo bitNo ->
            let
                oldValue =
                    model.operands
                        |> List.drop opNo
                        |> List.head
                        |> Maybe.withDefault 0

                bitMask =
                    shiftLeftBy bitNo 1

                newVal =
                    Bitwise.xor oldValue bitMask

                newOperands =
                    model.operands
                        |> List.indexedMap
                            (\idx oldVal ->
                                if idx == opNo then
                                    newVal

                                else
                                    oldVal
                            )
            in
            ( { model | operands = newOperands }, Cmd.none )

        OperatorSwitch ->
            ( { model
                | operator =
                    case model.operator of
                        And ->
                            Or

                        Or ->
                            Xor

                        Xor ->
                            Not

                        Not ->
                            And
              }
            , Cmd.none
            )


bitClass : Int -> String
bitClass bitNo =
    if bitNo == 15 then
        "bit left"

    else if bitNo == 0 then
        "bit right"

    else
        "bit"


viewBit : Int -> Maybe Int -> Int -> Html Msg
viewBit value reg bitNo =
    let
        bitValue =
            and (shiftRightZfBy bitNo value) 1

        bitClassNames =
            bitClass bitNo

        regClassNames =
            case reg of
                Just regNo ->
                    "reg reg" ++ String.fromInt regNo

                Nothing ->
                    "result"

        classNames =
            regClassNames ++ " " ++ bitClassNames

        classAttribute =
            class classNames

        attributes =
            case reg of
                Just regNo ->
                    [ classAttribute, onClick (BitFlip regNo bitNo) ]

                Nothing ->
                    [ classAttribute ]
    in
    td attributes [ text (String.fromInt bitValue) ]


viewBits : Int -> Maybe Int -> List (Html Msg)
viewBits value reg =
    List.range 0 15
        |> List.reverse
        |> List.map (viewBit value reg)


viewValue : Int -> Maybe Int -> List (Html Msg)
viewValue value reg =
    viewBits value reg
        ++ [ td [ class "equal" ] [ text "=" ]
           , td [ class "number" ] [ text (toHex value) ]
           , td [ class "equal" ] [ text "=" ]
           , td [ class "number" ] [ text (String.fromInt value) ]
           ]


viewOperand : Int -> Int -> Html Msg
viewOperand idx value =
    tr []
        (th []
            [ text ("Operant " ++ String.fromInt (idx + 1) ++ ":") ]
            :: viewValue value (Just idx)
        )


calculateResult : Model -> Int
calculateResult model =
    case model.operator of
        And ->
            List.foldr Bitwise.and 0xFFFF model.operands

        Or ->
            List.foldr Bitwise.or 0x00 model.operands

        Xor ->
            List.foldr Bitwise.xor 0x00 model.operands

        Not ->
            List.take 1 model.operands
            |> List.foldr Bitwise.xor 0xFFFF


view : Model -> Html Msg
view model =
    table [ class "operands" ]
        [ thead []
            [ tr []
                [ th [] []
                , td [ colspan 16 ] [ text "Binärdarstellung" ]
                , td [ colspan 2, class "numberhead" ] [ text "hexadezimal" ]
                , td [ colspan 2, class "numberhead" ] [ text "dezimal" ]
                ]
            , tr []
                [ th [] []
                , td [ colspan 16, class "howto" ] [ text "⇊ Klick auf die Bits der Operanten um sie zu ändern! ⇊" ]
                ]
            ]
        , tbody []
            ((List.take
                (if model.operator == Not then
                    1

                 else
                    2
                )
                model.operands
                |> List.indexedMap viewOperand
             )
                ++ [ tr []
                        (th [ onClick OperatorSwitch ]
                            [ text (operatorName model ++ "-Ergebnis:") ]
                            :: viewValue (calculateResult model)
                                Nothing
                        )
                   ]
            )
        ]


subscriptions : Model -> Sub msg
subscriptions model =
    Sub.none


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
