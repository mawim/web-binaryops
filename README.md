Modern replacement for my Applets in early blog postings
========================================================

I still have some Java applets on my site where I showed how the binary
operations AND, OR, and NOT work. I want to replace them with something, that
works on current browsers.



Hint
----

* `elm make src/Operands.elm --output operands.js`
